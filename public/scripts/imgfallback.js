$('.img-with-fallback').on("error", function () {
    let obj = $(this);
    if (obj.attr("src") !== obj.data("fallback"))
        obj.attr("src", obj.data("fallback"));
});