var e = document.querySelector("#season-select");
if (e)
    e.onchange = function (e) {
        window.location = e.target.value;
    };

e = document.querySelector("#session-select");
if (e)
    e.onchange = function (e) {
        window.location = e.target.value;
    };

e = document.querySelector("#player-edit-select");
if (e) {
    e.onchange = function (e) {
        window.location = e.target.value;
    };
}
