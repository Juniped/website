alter table game_actions
    add column action_stamp timestamp not null default current_timestamp;