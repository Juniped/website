alter table scoring_plays
    add column game_id int null after game_action,
    add foreign key fk_scoring_plays_game_id (game_id) references games (id);

insert into scoring_plays (game_action, game_id, scorer1, scorer2, scorer3, scorer4, description)
select id,
       game_id,
       SUBSTRING_INDEX(SUBSTRING_INDEX(scorers, ',', 1), ',', -1),
       case when length(scorers) - length(replace(scorers, ',', '')) > 0 then SUBSTRING_INDEX(SUBSTRING_INDEX(scorers, ',', 2), ',', -1) else null end,
       case when length(scorers) - length(replace(scorers, ',', '')) > 1 then SUBSTRING_INDEX(SUBSTRING_INDEX(scorers, ',', 3), ',', -1) else null end,
       case when length(scorers) - length(replace(scorers, ',', '')) > 2 then SUBSTRING_INDEX(SUBSTRING_INDEX(scorers, ',', 4), ',', -1) else null end,
       'cloned'
from game_actions
where scorers is not null
on duplicate key update game_action=game_action;

update scoring_plays a
set game_id=(select game_id from game_actions where id = a.game_action)
where game_id is null;

alter table scoring_plays
    modify game_id int not null;