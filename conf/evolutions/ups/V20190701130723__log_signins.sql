create table signin_logs (
    id int not null auto_increment primary key,
    user int not null,
    method varchar(32) not null,
    ip text not null,
    stamp timestamp not null default current_timestamp,
    foreign key fk_signin_history_user (user) references users(id)
);