-- create the table alterations
create table game_states
(
    id                    int not null auto_increment primary key,
    away_batting_position int not null,
    home_batting_position int not null,
    r1                    int null,
    r2                    int null,
    r3                    int null,
    r1_responsibility     int null,
    r2_responsibility     int null,
    r3_responsibility     int null,
    outs                  int not null,
    inning                int not null,
    score_away            int not null,
    score_home            int not null,
    foreign key fk_game_state_r1 (r1) references players (id),
    foreign key fk_game_state_r1_responsibility (r1_responsibility) references players (id),
    foreign key fk_game_state_r2 (r2) references players (id),
    foreign key fk_game_state_r2_responsibility (r2_responsibility) references players (id),
    foreign key fk_game_state_r3 (r3) references players (id),
    foreign key fk_game_state_r3_responsibility (r3_responsibility) references players (id)
);

alter table game_actions
    add column before_state int null,
    add column after_state int null,
    add foreign key fk_game_action_before_state (before_state) references game_states (id),
    add foreign key fk_game_action_after_state (after_state) references game_states (id);

-- first copy over all game actions' result portion
insert into game_states (id, away_batting_position, home_batting_position, r1, r2, r3, outs, inning, score_away,
                         score_home)
select id,
       result_away_batting_position,
       result_home_batting_position,
       result_player_on_first,
       result_player_on_second,
       result_player_on_third,
       result_outs,
       result_inning,
       result_score_away,
       result_score_home
from game_actions;

-- now update the auto_inc value
select @max := MAX(id) + 1 from game_states;
select @sql := CONCAT('alter table game_states auto_increment = ', @max);
prepare auto_inc_stmt from @sql;
execute auto_inc_stmt;
deallocate prepare auto_inc_stmt;

-- after state should be the play's id
-- before state requires back-lookup, and can't be done for game-init actions
update game_actions a
set a.after_state=a.id,
    a.before_state=(select b.id from (select b.id from game_actions b where a.id = b.replaced_by) b);

-- now remove the no-longer-needed game-init "plays"
delete
from game_actions
where play_type = 0
  and result_inning = 1;

-- and bridge the gap on the extra-inning "plays"
update game_actions a, game_actions b
set a.after_state=b.after_state
where b.play_type = 0
  and b.result_inning != 1;

delete
from game_actions
where play_type = 0
  and result_inning = 1;

-- remove the result columns from the game_actions table
alter table game_actions
    drop column result_away_batting_position,
    drop column result_home_batting_position,
    drop foreign key fk_game_action_player_on_first,
    drop foreign key fk_game_action_player_on_second,
    drop foreign key fk_game_action_player_on_third,
    drop column result_player_on_first,
    drop column result_player_on_second,
    drop column result_player_on_third,
    drop column result_outs,
    drop column result_score_away,
    drop column result_score_home,
    drop column result_inning;

-- add a new column on games to track current state
alter table games
    add column state int null,
    add foreign key fk_games_state (state) references game_states (id);

-- link current states to games
update games a
set state=(select after_state from game_actions where game_id = a.id and replaced_by is null);

-- create an init-style game state for games that haven't started (they can all share a reference)
insert into game_states (away_batting_position, home_batting_position, r1, r2, r3, r1_responsibility, r2_responsibility,
                         r3_responsibility, outs, inning, score_away, score_home)
values (1, 1, null, null, null, null, null, null, 0, 1, 0, 0);

update games a
set state=(select id
           from game_states
           where away_batting_position = 1
             and home_batting_position = 1
             and r1 is null
             and r2 is null
             and r3 is null
             and r1_responsibility is null
             and r2_responsibility is null
             and r3_responsibility is null
             and outs = 0
             and inning = 1
             and score_away = 0
             and score_home = 0
           limit 1)
where state is null;

-- now remove nullability on game states
alter table game_actions
    modify after_state int not null,
    modify before_state int not null;

alter table games
    modify state int not null;