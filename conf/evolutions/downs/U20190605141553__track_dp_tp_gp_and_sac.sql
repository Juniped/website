ALTER TABLE batting_stats
    DROP COLUMN total_dp,
    DROP COLUMN total_tp,
    DROP COLUMN total_gp,
    DROP COLUMN total_sac;

ALTER TABLE pitching_stats
    DROP COLUMN total_dp,
    DROP COLUMN total_tp,
    DROP COLUMN total_gp,
    DROP COLUMN total_sac;
