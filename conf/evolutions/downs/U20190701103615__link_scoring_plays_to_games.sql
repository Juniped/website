alter table scoring_plays
    drop foreign key fk_scoring_plays_game_id,
    drop column game_id;
