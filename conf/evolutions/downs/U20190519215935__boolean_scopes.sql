ALTER TABLE users
    ADD COLUMN scopes VARCHAR(1024) NOT NULL DEFAULT 'player',
    DROP COLUMN is_player,
    DROP COLUMN is_umpire,
    DROP COLUMN is_commissioner;
