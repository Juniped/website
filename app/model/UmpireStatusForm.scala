package model

import play.api.data._
import play.api.data.Forms._

case class UmpireStatusForm(user: String, newUmpStatus: Boolean)

object UmpireStatusForm {

  def umpireStatusForm(implicit db: FBDatabase) = Form(
    mapping(
      "user" -> nonEmptyText.transform[String]("/u/" + _, _.substring(3)).verifying("This user does not exist.", db.getUserByRedditName(_).isDefined),
      "status" -> boolean
    )(UmpireStatusForm.apply)(UmpireStatusForm.unapply)
  )

}
