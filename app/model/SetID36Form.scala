package model

import play.api.data._
import play.api.data.Forms._

case class SetID36Form(id36: String)

object SetID36Form {

  private val REDDIT_ID36_REGEX = """.*?reddit\.com/r/fakebaseball/comments/([0-9a-zA-Z]{6,7})/.*""".r

  val setID36Form = Form(
    mapping(
      "id36" -> nonEmptyText.verifying("This doesn't look like a reddit link or ID36.", link => {
        link match {
          case REDDIT_ID36_REGEX(_*) => true
          case _ => (link.length == 6 || link.length == 7) && link.forall(_.isLetterOrDigit)
        }
      })
    )(provided => {
      val id36 = provided match {
        case REDDIT_ID36_REGEX(matchedID36) => matchedID36
        case link => link
      }
      SetID36Form.apply(id36)
    })(SetID36Form.unapply)
  )

}
