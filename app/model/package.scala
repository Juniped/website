import java.sql.Timestamp
import java.util.Date

import play.api.libs.json.Writes
import services.StatCalculatorService

import scala.language.implicitConversions

package object model {

  implicit val DATE_TIME_WRITE: Writes[Date] = play.api.libs.json.Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")

  val availablePositions: Seq[String] = Seq(
    "P",
    "C",
    "1B",
    "2B",
    "3B",
    "SS",
    "LF",
    "CF",
    "RF"
  )

  val lineupPositions: Set[String] = availablePositions.toSet + "DH" + "PH"

  val PLAY_TYPE_SWING: Int = 1
  val PLAY_TYPE_AUTO_K: Int = 2
  val PLAY_TYPE_AUTO_BB: Int = 3
  val PLAY_TYPE_BUNT: Int = 4
  val PLAY_TYPE_INFIELD_IN: Int = 5
  val PLAY_TYPE_STEAL: Int = 6
  val PLAY_TYPE_MULTI_STEAL: Int = 7
  val PLAY_TYPE_IBB: Int = 8

  val PLAY_TYPES: Map[String, Int] = Map(
    "Swing" -> PLAY_TYPE_SWING,
    "Auto K" -> PLAY_TYPE_AUTO_K,
    "Auto BB" -> PLAY_TYPE_AUTO_BB,
    "Bunt" -> PLAY_TYPE_BUNT,
    "Infield In" -> PLAY_TYPE_INFIELD_IN,
    "Steal" -> PLAY_TYPE_STEAL,
    "Multi-Steal" -> PLAY_TYPE_MULTI_STEAL,
    "IBB" -> PLAY_TYPE_IBB,
  )

  import Tables._

  // Table mappings to better names
//  type BattingStatSet = BattingStatsRow // This table crossed 22 columns and requires an HList <-> class mapping
  type BattingType = BattingTypesRow
  type CommitteeItem = CommitteeItemsRow
  type CommitteeItemMotioner = CommitteeItemMotionersRow
  type CommitteeItemVote = CommitteeItemVotesRow
  type DiscordPing = DiscordPingsRow
  type Game = GamesRow
  type GameAction = GameActionsRow
  type GameAwardsSet = GameAwardsRow
  type GameState = GameStatesRow
  type GlobalSetting = GlobalSettingsRow
  type GMAssignment = GmAssignmentsRow
  type Lineup = LineupsRow
  type LineupEntry = LineupEntriesRow
  type Park = ParksRow
  type PitchingBonus = PitchingBonusesRow
  type PitchingStatSet = PitchingStatsRow
  type PitchingType = PitchingTypesRow
  type PlayerApplication = PlayerApplicationsRow
  type Player = PlayersRow
  type ScoringPlay = ScoringPlaysRow
  type Team = TeamsRow
  type UmpireAssignment = UmpireAssignmentsRow
  type User = UsersRow
  type UserPreferencesSet = UserPreferencesRow

  class BattingStatSet(val player: Int, val season: Int, val milr: Boolean, val totalPas: Int, val totalAbs: Int, val totalHr: Int, val total3b: Int, val total2b: Int, val total1b: Int, val totalBb: Int, val totalFo: Int, val totalK: Int, val totalPo: Int, val totalRgo: Int, val totalLgo: Int, val totalRbi: Int, val totalR: Int, val totalSb: Int, val totalCs: Int, val totalDp: Int, val totalTp: Int, val totalGp: Int, val totalSac: Int)
  implicit def battingStatHList2BattingStatSet(list: BattingStatsRow): BattingStatSet = {
    new BattingStatSet(
      list.head,
      list.tail.head,
      list.tail.tail.head,
      list.tail.tail.tail.head,
      list.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head,
      list.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.tail.head
    )
  }

  implicit def battingType2Range(bt: BattingType): CalcRange = CalcRange(bt.rangeHr, bt.range3b, bt.range2b, bt.range1b, bt.rangeBb, bt.rangeFo, bt.rangeK, bt.rangePo, bt.rangeRgo, bt.rangeLgo)
  implicit def pitchingType2Range(pt: PitchingType): CalcRange = CalcRange(pt.rangeHr, pt.range3b, pt.range2b, pt.range1b, pt.rangeBb, pt.rangeFo, pt.rangeK, pt.rangePo, pt.rangeRgo, pt.rangeLgo)
  implicit def pitchingBonus2Range(pb: PitchingBonus): CalcRange = CalcRange(pb.rangeHr, pb.range3b, pb.range2b, pb.range1b, pb.rangeBb, pb.rangeFo, pb.rangeK, pb.rangePo, pb.rangeRgo, pb.rangeLgo)
  case class SwingResult(diff: Option[Int], result: String, resultMin: Option[Int], resultMax: Option[Int])

  implicit class GameActionExtensions(recentPlay: GameAction) {
    def preview: String = {
      (
        recentPlay.playType match {
          case PLAY_TYPE_BUNT => "Bunt"
          case PLAY_TYPE_AUTO_K => "Auto K"
          case PLAY_TYPE_AUTO_BB => "Auto BB"
          case PLAY_TYPE_STEAL | PLAY_TYPE_MULTI_STEAL => "Steal"
          case _ => "Swing"
        }
      ) + s": ${recentPlay.swing.map(_.toString).getOrElse("x")}  \nPitch: ${recentPlay.pitch.map(_.toString).getOrElse("x")}  \n${recentPlay.diff.map(_.toString).getOrElse("x")} -> ${recentPlay.result.getOrElse("Unknown")}"
    }
  }

  case class PartialGameAction(playType: Int, pitcher: Int, pitch: Option[Int], batter: Int, swing: Option[Int], diff: Option[Int], result: Option[String], runsScored: Int, scorers: Option[String], outsTracked: Int) {
    def toGameAction(game: Game, beforeState: Int, afterState: Int): GameAction = new GameAction(0, None, game.id, playType, Some(pitcher), pitch, Some(batter), swing, diff, result, runsScored, scorers, outsTracked, beforeState, afterState, new Timestamp(System.currentTimeMillis))
  }

  implicit class BattingStatsExtensions(stats: BattingStatSet) {
    def totalH: Int = StatCalculatorService.calculateHits(stats)
    def ba: Float = StatCalculatorService.calculateBA(stats)
    def obp: Float = StatCalculatorService.calculateOBP(stats)
    def slg: Float = StatCalculatorService.calculateSLG(stats)
    def ops: Float = StatCalculatorService.calculateOPS(stats)
  }

  implicit class PitchingStatsExtensions(stats: PitchingStatSet) {
    def totalIP: Float = StatCalculatorService.calculateIP(stats)
    def era: Float = StatCalculatorService.calculateERA(stats)
    def whip: Float = StatCalculatorService.calculateWHIP(stats)
    def totalGo: Int = StatCalculatorService.calculateGO(stats)
  }

  implicit class PlayerExtensions(player: Player) {
    def fullName: String = s"${player.firstName.getOrElse("")} ${player.lastName}".trim
  }

  implicit class GameStateExtensions(state: GameState) {
    def inningStr: String = {
      val i = state.inning
      if(i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}"
    }
  }

  // Common combo types
  type PlayerWithUserTuple = (Player, User)
  case class PlayerWithUser(player: Player, user: User)
  def tuple2PlayerWithUser(tuple: PlayerWithUserTuple): PlayerWithUser = PlayerWithUser(tuple._1, tuple._2)

  type UserWithPlayerTuple = (User, Option[Player])
  case class UserWithPlayer(user: User, player: Option[Player])
  def tuple2UserWithPlayer(tuple: UserWithPlayerTuple): UserWithPlayer = UserWithPlayer(tuple._1, tuple._2)

  type PlayerWithTypesAndInfoTuple = (Player, User, Option[Team], BattingType, Option[PitchingType], Option[PitchingBonus])
  case class PlayerWithTypesAndInfo(player: Player, user: User, team: Option[Team], battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus])
  def tuple2PlayerWithTypesAndInfo(tuple: PlayerWithTypesAndInfoTuple): PlayerWithTypesAndInfo = PlayerWithTypesAndInfo(tuple._1, tuple._2, tuple._3, tuple._4, tuple._5, tuple._6)

  type PlayerWithTypesTuple = (Player, BattingType, Option[PitchingType], Option[PitchingBonus])
  case class PlayerWithTypes(player: Player, battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus])
  def tuple2PlayerWithTypes(tuple: PlayerWithTypesTuple): PlayerWithTypes = PlayerWithTypes(tuple._1, tuple._2, tuple._3, tuple._4)

  type GameWithTeamsTuple = (Game, Team, Team, Park)
  case class GameWithTeams(game: Game, awayTeam: Team, homeTeam: Team, park: Park) {
    def name = s"${game.season}.${game.session} ${awayTeam.tag}@${homeTeam.tag}"
  }
  def tuple2GameWithTeams(tuple: GameWithTeamsTuple): GameWithTeams = GameWithTeams(tuple._1, tuple._2, tuple._3, tuple._4)

  type GameWithGameStateTuple = (Game, Team, Team, Park, GameState)
  case class GameWithGameState(game: Game, awayTeam: Team, homeTeam: Team, park: Park, state: GameState){
    def name = s"${game.season}.${game.session} ${awayTeam.tag}@${homeTeam.tag}"
    def awayBatting: Boolean = state.inning % 2 == 1
    def fieldingLineup: Option[Int] = if (awayBatting) game.homeLineup else game.awayLineup
    def battingLineup: Option[Int] = if (awayBatting) game.awayLineup else game.homeLineup
    def nextBatterLineupPos: Int = if (awayBatting) state.awayBattingPosition else state.homeBattingPosition
    def inning: String = state.inningStr
    def inningWithOuts: String = if (game.completed) "FINAL" else s"$inning ${state.outs} Out"
  }
  def tuple2GameWithGameState(tuple: GameWithGameStateTuple): GameWithGameState = GameWithGameState(tuple._1, tuple._2, tuple._3, tuple._4, tuple._5)

  type PlayerWithStatsTuple = (Player, User, Option[BattingStatsRow], Option[PitchingStatSet])
  case class PlayerWithStats(player: Player, user: User, battingStats: Option[BattingStatSet], pitchingStats: Option[PitchingStatSet])
  def tuple2PlayerWithStats(tuple: PlayerWithStatsTuple): PlayerWithStats = PlayerWithStats(tuple._1, tuple._2, tuple._3.map(battingStatHList2BattingStatSet), tuple._4)

  case class PlayerWithAllStats(player: Player, user: User, battingStats: Map[(Boolean, Int), BattingStatSet], pitchingStats: Map[(Boolean, Int), PitchingStatSet])

  type LineupEntryWithPlayerTuple = (LineupEntry, PlayerWithStatsTuple)
  case class LineupEntryWithPlayer(lineupEntry: LineupEntry, player: PlayerWithStats)
  def tuple2LineupWithPlayers(tuple: LineupEntryWithPlayerTuple): LineupEntryWithPlayer = LineupEntryWithPlayer(tuple._1, tuple2PlayerWithStats(tuple._2))

  type CommitteeItemWithAuthorTuple = (CommitteeItem, User, String)
  case class CommitteeItemWithAuthor(item: CommitteeItem, user: User, author: String)
  def tuple2CommitteeItemWithAuthor(tuple: CommitteeItemWithAuthorTuple): CommitteeItemWithAuthor = CommitteeItemWithAuthor(tuple._1, tuple._2, tuple._3)

  type CommitteeItemMotionerWithUserTuple = (CommitteeItemMotioner, User, String)
  case class CommitteeMotionerWithUser(user: User, name: String)
  def tuple2CommitteeMotionerWithUser(tuple: CommitteeItemMotionerWithUserTuple): CommitteeMotionerWithUser = CommitteeMotionerWithUser(tuple._2, tuple._3)

  type GameActionWithPlayersTuple = (GameAction, Player, Player)
  case class GameActionWithPlayers(gameAction: GameAction, batter: Player, pitcher: Player)
  def tuple2GameActionWithPlayers(tuple: GameActionWithPlayersTuple): GameActionWithPlayers = GameActionWithPlayers(tuple._1, tuple._2, tuple._3)

  type GameActionWithStatesAndPlayersTuple = (GameAction, Player, Player, GameState, GameState)
  case class GameActionWithStatesAndPlayers(gameAction: GameAction, batter: Player, pitcher: Player, beforeState: GameState, afterState: GameState)
  def tuple2GameActionWithStatesAndPlayers(tuple: GameActionWithStatesAndPlayersTuple): GameActionWithStatesAndPlayers = GameActionWithStatesAndPlayers(tuple._1, tuple._2, tuple._3, tuple._4, tuple._5)

  type UserWithPreferencesTuple = (User, UserPreferencesSet)
  case class UserWithPreferences(user: User, preferences: UserPreferencesSet)
  def tuple2UserWithPreferences(tuple: UserWithPreferencesTuple): UserWithPreferences = UserWithPreferences(tuple._1, tuple._2)

  type ScoringPlayWithActionAndStatesTuple = (ScoringPlay, GameAction, GameState, GameState)
  case class ScoringPlayWithActionAndStates(scoringPlay: ScoringPlay, gameAction: GameAction, beforeState: GameState, afterState: GameState)
  def tuple2ScoringPlayWithActionAndStates(tuple: ScoringPlayWithActionAndStatesTuple): ScoringPlayWithActionAndStates = ScoringPlayWithActionAndStates(tuple._1, tuple._2, tuple._3, tuple._4)

  type TeamWithParkTuple = (Team, Park)
  case class TeamWithPark(team: Team, park: Park)
  def tuple2TeamWithPark(tuple: TeamWithParkTuple): TeamWithPark = TeamWithPark(tuple._1, tuple._2)

  type GameActionWithExtendedInfoTuple = (GameAction, Player, Player, GameState, GameState, Game, User, User, Team, Team, Park)
  case class GameActionWithExtendedInfo(gameAction: GameAction, batter: PlayerWithUser, pitcher: PlayerWithUser, beforeState: GameState, afterState: GameState, game: GameWithTeams)
  def tuple2GameActionWithExtendedInfo(tuple: GameActionWithExtendedInfoTuple): GameActionWithExtendedInfo = GameActionWithExtendedInfo(tuple._1, tuple2PlayerWithUser((tuple._2, tuple._7)), tuple2PlayerWithUser((tuple._3, tuple._8)), tuple._4, tuple._5, tuple2GameWithTeams((tuple._6, tuple._9, tuple._10, tuple._11)))

  type GameAwardsWithPlayersTuple = (GameAwardsSet, Player, Player, Player, Option[Player])
  case class GameAwardsWithPlayers(gameAwards: GameAwardsSet, winningPitcher: Player, losingPitcher: Player, playerOfTheGame: Player, save: Option[Player])
  def tuple2GameAwardsWithPlayers(tuple: GameAwardsWithPlayersTuple): GameAwardsWithPlayers = GameAwardsWithPlayers(tuple._1, tuple._2, tuple._3, tuple._4, tuple._5)

  type ParkWithTeamTuple = (Park, Option[Team])
  case class ParkWithTeam(park: Park, team: Option[Team])
  def tuple2ParkWithTeam(tuple: ParkWithTeamTuple): ParkWithTeam = ParkWithTeam(tuple._1, tuple._2)

  private val DEFAULT_UMP_BATTER_PING_FORMAT = "%awayTag% %awayScore% - %homeScore% %homeTag% | %basesDiamonds% | %inning% %outs% Out\n\n%batterTeamTag% %batterPosition% %batterPing%: %batterRecord%\n\n"
  implicit class UserPreferencesExtensions(prefs: UserPreferencesSet) {
    def withDefaults: UserPreferencesSet = prefs.copy(
      umpBatterPing = Some(prefs.umpBatterPing.getOrElse(DEFAULT_UMP_BATTER_PING_FORMAT))
    )
  }

  def sortPlayersByName(a: Player, b: Player): Boolean = a.fullName.compareTo(b.fullName) < 0

  private val POSITIONS_ORDER = Array("GM", "P", "C", "1B", "2B", "3B", "SS", "LF", "CF", "RF")
  def sortPlayersByPosition(a: Player, b: Player): Boolean = POSITIONS_ORDER.indexOf(a.positionPrimary).compareTo(POSITIONS_ORDER.indexOf(b.positionPrimary)) < 0
  def sortPlayersByPosition(a: PlayerWithTypesAndInfo, b: PlayerWithTypesAndInfo): Boolean = POSITIONS_ORDER.indexOf(a.player.positionPrimary).compareTo(POSITIONS_ORDER.indexOf(b.player.positionPrimary)) < 0

}
