package model

import play.api.data._
import play.api.data.Forms._

case class EditScoringPlayForm(gameAction: Int, newDescription: String)

object EditScoringPlayForm {

  val editScoringPlayForm = Form(
    mapping(
      "gameAction" -> number(min = 1),
      "newDescription" -> nonEmptyText()
    )(EditScoringPlayForm.apply)(EditScoringPlayForm.unapply)
  )

}