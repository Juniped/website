import com.google.inject.AbstractModule
import model.FBDatabase
import services.RedditAuthService

class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[RedditAuthService])
    bind(classOf[FBDatabase])
  }

}
