package controllers

import javax.inject.{Inject, Singleton}
import model.CreatePlayerForm._
import model.EditParkForm._
import model.EditPlayerForm._
import model.EditTeamForm._
import model.EndGameForm._
import model.FBDatabase
import model.NewGameForm._
import model.UmpireAssignmentForm._
import model.UmpireStatusForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

import scala.util.Try

@Singleton
class AdminController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def adminPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.index(db.countPendingPlayerApplications()))
  }

  def listPendingApplications: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.applications(db.getAllPendingApplications))
  }

  def showNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    Ok(views.html.admin.newgame(teams, newGameForm, db.getAllParks, mlr))
  }

  def submitNewGameForm(mlr: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val teams = if (mlr) db.getMLRTeams else db.getMiLRTeams
    newGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.newgame(teams, formWithErrors, db.getAllParks, mlr)),
      formData => {
        db.createGame(formData)
        Redirect(routes.AdminController.showNewGameForm(mlr))
      }
    )
  }

  def showUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.manageumpires(db.getAllActiveUmpires, umpireStatusForm))
  }

  def submitUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireStatusForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.manageumpires(db.getAllActiveUmpires, formWithErrors)),
      formData => {
        db.setUmpireStatus(db.getUserByRedditName(formData.user).get.id, formData.newUmpStatus)
        Redirect(routes.AdminController.showUmpireStatusForm())
      }
    )
  }

  def showUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnfinishedGames, db.getGamesWithUmpAssignments, umpireAssignmentForm))
  }

  def submitUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireAssignmentForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.umpireassignments(db.getAllActiveUmpires, db.getUnfinishedGames, db.getGamesWithUmpAssignments, formWithErrors)),
      formData => {
        val ump = db.getUserByRedditName(formData.umpire).get
        if (formData.addition)
          db.assignUmpireToGame(ump, formData.game)
        else
          db.removeUmpireFromGame(ump, formData.game)
        Redirect(routes.AdminController.showUmpireAssignmentForm())
      }
    )
  }

  def viewEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.endgame(db.getActiveGames, endGameForm))
  }

  def submitEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    endGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.endgame(db.getActiveGames, formWithErrors)),
      formData => {
        db.markGameComplete(formData.gameId)
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def showEditPlayerDirectory: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editplayerlist(db.getAllPlayerNames))
  }

  def showEditPlayerForm(id: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getPlayerWithTypes(id).map { player =>
      Ok(views.html.admin.editplayer(player, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses, editPlayerForm))
    } getOrElse {
      NotFound("This player does not exist.")
    }
  }

  def submitEditPlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editPlayerForm.bindFromRequest.fold(
      formWithErrors => {
        formWithErrors.data.get("id").flatMap(idStr => Try(idStr.toInt).toOption).flatMap { id =>
          db.getPlayerWithTypes(id).map { player =>
            BadRequest(views.html.admin.editplayer(player, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses, formWithErrors))
          }
        } getOrElse {
          BadRequest(views.html.admin.editplayerlist(db.getAllPlayerNames))
        }
      },
      formData => {
        db.editPlayer(formData)
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def showCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.createplayer(createPlayerForm, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses))
  }

  def submitCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    createPlayerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.createplayer(formWithErrors, db.getAllTeams, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses)),
      formData => {
        val user = db.getUserByRedditName(formData.redditName).map(_.id).getOrElse(db.createUserAccount(formData.redditName))
        val id = db.createPlayer(user, formData)
        Redirect(routes.PlayerController.showPlayer(id))
      }
    )
  }

  def showEditParkSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editparkselector(db.getAllParks))
  }

  def showEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val park = parkId.flatMap(db.getParkById)
    park.map { park =>
      val form = editParkForm.bind(Map(
        "name" -> park.name,
        "factorHR" -> park.factorHr.toString,
        "factor3B" -> park.factor3b.toString,
        "factor2B" -> park.factor2b.toString,
        "factor1B" -> park.factor1b.toString,
        "factorBB" -> park.factorBb.toString,
      ))
        .discardingErrors
      Ok(views.html.admin.editpark(parkId, form))
    } getOrElse {
      Ok(views.html.admin.editpark(parkId, editParkForm))
    }
  }

  def submitEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editParkForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editpark(parkId, formWithErrors)),
      formData => {
        val newId: Int = parkId.map { parkId =>
          db.editPark(parkId, formData)
          parkId
        } getOrElse {
          db.createPark(formData)
        }
        Redirect(routes.AdminController.showEditParkForm(Some(newId)))
      }
    )
  }

  def showEditTeamSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editteamselector(db.getAllTeams))
  }

  def showEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val team = teamId.flatMap(db.getTeamById)
    team.map { team =>
      val form = editTeamForm.bind(Map(
        "tag" -> team.tag,
        "name" -> team.name,
        "park" -> team.park.toString,
        "colorDiscord" -> team.colorDiscord.toString,
        "colorRoster" -> team.colorRoster.toString,
        "colorRosterBg" -> team.colorRosterBg.toString,
        "milr" -> team.milr.toString,
        "milrTeam" -> team.milrTeam.map(_.toString).getOrElse("")
      ))
        .discardingErrors
      Ok(views.html.admin.editteam(Some(team.id), form, db.getAllParks, db.getMiLRTeams))
    } getOrElse {
      Ok(views.html.admin.editteam(None, editTeamForm, db.getAllParks, db.getMiLRTeams))
    }
  }

  def submitEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editTeamForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editteam(teamId, formWithErrors, db.getAllParks, db.getMiLRTeams)),
      formData => {
        val newId: Int = teamId.map { teamId =>
          db.editTeam(teamId, formData)
          teamId
        } getOrElse {
          db.createTeam(formData)
        }
        Redirect(routes.AdminController.showEditTeamForm(Some(newId)))
      }
    )
  }

}
