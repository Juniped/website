package controllers

import javax.inject._
import model.FBDatabase
import play.api.mvc._
import services.{DiscordAuthService, GlobalSettingsProvider, RedditAuthService}

import scala.collection.mutable
import scala.util.Random

@Singleton
class AuthController @Inject()(redditAuthService: RedditAuthService, discordAuthService: DiscordAuthService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  private val states = mutable.Set[String]()
  private val discordStates = mutable.Set[String]()
  private val random = new Random()

  def redirectToSignIn(path: String): Action[AnyContent] = Action { implicit req =>
    val randomInt = random.nextInt()
    val newState = Integer.toString(randomInt, 36)

    states.add(newState)
    Redirect(redditAuthService.oauthConsentUri(newState))
      .addingToSession("auth_state" -> newState)
      .addingToSession("redirection_target" -> path)
  }

  def signIn: Action[AnyContent] = Action { implicit req =>
    req.getQueryString("error").map { error =>
      BadRequest(s"Got an error from Reddit: $error\n\nPlease try again later.")
    } getOrElse {
      req.getQueryString("state").flatMap { state =>
        if (states.contains(state) && req.session.get("auth_state").contains(state)) {
          states.remove(state)
          req.getQueryString("code").map { code =>
            val redditAccount = redditAuthService.getIdentity(code)
            val userId = db.getUserByRedditName(redditAccount.name).map(_.id).getOrElse(db.createUserAccount(redditAccount.name))
            db.logSignIn(userId, "reddit_oauth", req.headers.get("X-Real-IP").getOrElse(req.remoteAddress))
            Redirect(req.session.get("redirection_target").getOrElse("/"))
              .withNewSession
              .withSession("uid" -> userId.toString)
          }
        } else {
          Some(BadRequest("Bad request."))
        }
      } getOrElse {
        BadRequest("Bad request.")
      }
    }
  }

  def signOut: Action[AnyContent] = Action {
    Redirect(routes.HomeController.index())
      .withNewSession
  }

  def redirectToDiscordSignIn: Action[AnyContent] = Action { implicit req =>
    val randomInt = random.nextInt()
    val newState = Integer.toString(randomInt, 36)

    discordStates.add(newState)
    Redirect(discordAuthService.oauthConsentUri(newState))
      .addingToSession("discord_state" -> newState)
  }

  def setDiscord: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    ru.getQueryString("error").map { error =>
      BadRequest(s"Got an error from Discord: $error\n\nPlease try again later.")
    } getOrElse {
      ru.getQueryString("state").flatMap { state =>
        if (discordStates.contains(state) && ru.session.get("discord_state").contains(state)) {
          discordStates.remove(state)
          ru.getQueryString("code").map { code =>
            val identity = discordAuthService.getIdentity(code)
            db.setDiscordSnowflake(ru, Some(identity.id))
            Redirect(routes.AccountController.viewAccount(ru._2.id))
              .removingFromSession("discord_state")(ru)
          }
        } else {
          Some(BadRequest("Bad request."))
        }
      } getOrElse {
        BadRequest("Bad request.")
      }
    }
  }

}
