package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.NewPlayerForm.newPlayerForm
import model.PlayerApplicationResponseForm.playerApplicationResponseForm
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class PlayerApplicationController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def showNewPlayerForm: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    val form = db.getPlayerApplicationByUser(ru).map { app =>
      newPlayerForm.bind(Map(
        "returning" -> app.isReturningPlayer.toString,
        "firstName" -> app.firstName.getOrElse(""),
        "lastName" -> app.lastName,
        "willingToJoinDiscord" -> app.isWillingToJoinDiscord.toString,
        "positionPrimary" -> app.positionPrimary,
        "positionSecondary" -> app.positionSecondary.getOrElse(""),
        "rightHanded" -> app.isRightHanded.toString,
        "battingType" -> db.getBattingTypeById(app.battingType).shortcode,
        "pitchingType" -> app.pitchingType.map(db.getPitchingTypeById).map(_.shortcode).getOrElse(""),
        "pitchingBonus" -> app.pitchingBonus.map(db.getPitchingBonusById).map(_.shortcode).getOrElse(""),
      )).discardingErrors
    } getOrElse {
      newPlayerForm
    }
    Ok(views.html.players.newplayer(form))
  }

  def createOrEditNewPlayerApplication: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    newPlayerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.players.newplayer(formWithErrors)),
      formData => {
        val app = db.getPlayerApplicationByUser(ru).map { currentApp =>
          db.editPlayerApplication(currentApp.id, formData)
          currentApp
        } getOrElse {
          db.createPlayerApplication(formData)
        }
        Redirect(routes.PlayerApplicationController.showApplication(app.id))
      }
    )
  }

  def showApplication(id: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    db.getPlayerApplicationById(id).filter(_.user == ru._2.id || ru.isCommissioner).map { app =>
      Ok(views.html.players.viewapplication(app, db.getUserById(app.user).get, db.getBattingTypeById(app.battingType), app.pitchingType.map(db.getPitchingTypeById), app.pitchingBonus.map(db.getPitchingBonusById), playerApplicationResponseForm))
    } getOrElse {
      BadRequest("You don't have permission to view that.")
    }
  }

  def respondToApplication(id: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    db.getPlayerApplicationById(id).filter(_.status == 0).map { app =>
      playerApplicationResponseForm.bindFromRequest.fold(
        formWithErrors => Ok(views.html.players.viewapplication(app, db.getUserById(app.user).get, db.getBattingTypeById(app.battingType), app.pitchingType.map(db.getPitchingTypeById), app.pitchingBonus.map(db.getPitchingBonusById), formWithErrors)),
        formData => {
          if (formData.approval) {
            db.updatePlayerApplicationWithResponse(id, 2, None, ru._2.id)
            db.createNewPlayer(app)
          } else {
            db.updatePlayerApplicationWithResponse(id, 1, formData.rejectionMessage, ru._2.id)
          }
          Redirect(routes.PlayerApplicationController.showApplication(id))
        }
      )
    } getOrElse {
      BadRequest("This application does not exist.")
    }
  }

}
