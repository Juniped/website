package controllers

import javax.inject.{Inject, Singleton}
import model._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class TeamController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def listTeams: Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.teams.list(db.getMLRTeams, db.getMiLRTeams)(req, user))
  }

  def viewTeam(tag: String): Action[AnyContent] = messagesActionBuilder { implicit req =>
    db.getTeamByTag(tag).map { team =>
      val park = db.getParkForTeam(team)
      Ok(views.html.teams.view(TeamWithPark(team, park), db.getPlayersWithInfoOnTeam(team).sortWith(sortPlayersByPosition), db.getGMOfTeam(team))(req, user))
    } getOrElse {
      NotFound("That team does not exist.")
    }
  }

}
