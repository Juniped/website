package controllers

import javax.inject.{Inject, Singleton}
import model.NewCommitteeItemForm._
import model.{FBDatabase, NewCommitteeItemForm, User}
import org.jsoup.Jsoup
import org.jsoup.nodes.Document.OutputSettings
import org.jsoup.safety.Whitelist
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{GlobalSettingsProvider, MarkdownFormatterService}

@Singleton
class CommitteeController @Inject()(markdownService: MarkdownFormatterService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def viewAgenda: Action[AnyContent] = messagesActionBuilder { implicit req =>
    implicit val _user: Option[User] = super.user
    if (_user.exists(_.isCommittee))
      Ok(views.html.committee.list(db.getAllCommitteeItems, db.getVotingCommitteeItems))
    else
      Ok(views.html.committee.list(db.getPublicCommitteeItems))
  }

  def showNewItemForm(id: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMITTEE) { implicit ru =>
    db.getCommitteeItemById(id).map { item =>
      if (item.item.author == ru._2.id)
        Ok(views.html.committee.newitem(newCommitteeItemForm.fill(new NewCommitteeItemForm(itemId = id, title = item.item.title, body = item.item.body, isPrivate = item.item.isPrivate))))
      else
        Unauthorized("You are not allowed to edit this.")
    } getOrElse {
      Ok(views.html.committee.newitem(newCommitteeItemForm))
    }
  }

  def deleteItem(id: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMITTEE) { implicit ru =>
    db.getCommitteeItemById(id).map { item =>
      if (item.item.author == ru._2.id) {
        db.deleteCommitteeItem(item.item)
        Redirect(routes.CommitteeController.viewAgenda())
      } else {
        Unauthorized("Only the author can delete this item.")
      }
    } getOrElse {
      NotFound("This item does not exist.")
    }
  }

  def submitNewItemForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMITTEE) { implicit ru =>
    newCommitteeItemForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.committee.newitem(formWithErrors)),
      formData => {
        // Strip out HTML to prevent XSS
        val unsafeBody = formData.body
        val safeBody = Jsoup.clean(unsafeBody, "", Whitelist.none(), new OutputSettings().prettyPrint(false))

        // Save the stripped body
        val newItemId =
          if (formData.itemId == 0)
            db.createNewCommitteeItem(formData.copy(body = safeBody), ru)
          else {
            db.updateCommitteeItem(formData.copy(body = safeBody), ru)
            formData.itemId
          }

        // Send to page
        Redirect(routes.CommitteeController.viewItem(newItemId))
      }
    )
  }

  def viewItem(itemId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    implicit val _user: Option[User] = super.user
    db.getCommitteeItemById(itemId)
      .filter(!_.item.isPrivate || _user.exists(_.isCommittee))
      .map { item =>
        val renderedBody = markdownService.parseMarkdown(item.item.body)
        Ok(views.html.committee.view(item, renderedBody, db.getCommitteeItemMotioners(item.item), db.getCommitteeItemVotes(item.item)))
      } getOrElse {
      Unauthorized("You don't have permission to view this.")
    }
  }

  def updateMotionToVote(itemId: Int, motioning: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMITTEE) { implicit ru =>
    db.getCommitteeItemById(itemId).map { item =>
      if (item.item.status == 0) {
        if (motioning)
          db.addMotionerToItem(item.item, ru)
        else
          db.removeMotionerFromItem(item.item, ru)
        if (db.getCommitteeItemMotioners(item.item).size >= 5) {
          db.updateCommitteeItemStatus(item.item, 1)
        }
      }
      Redirect(routes.CommitteeController.viewItem(itemId))
    } getOrElse {
      NotFound("This item does not exist.")
    }
  }

  def updateVote(itemId: Int, votingFor: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMITTEE) { implicit ru =>
    db.getCommitteeItemById(itemId).map { item =>
      if (item.item.status == 1) {
        db.addVoteToItem(item.item, ru, votingFor)
      }
      Redirect(routes.CommitteeController.viewItem(itemId))
    } getOrElse {
      NotFound("This item does not exist.")
    }
  }

}
