package controllers

import javax.inject.{Inject, Singleton}
import model._
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class APIController @Inject()(implicit  settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def gamesInSession(season: Int, session: Int): Action[AnyContent] = Action {
    Ok(Json.toJson(db.getGamesInSession(season, session)))
  }

  def getActiveGameForTeam(team: String): Action[AnyContent] = Action {
    db.getActiveGameForTeam(team).map { game =>
      Ok(Json.toJson(game))
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def getGameLog(game: Int): Action[AnyContent] = Action {
    Ok(Json.toJson(db.getExtendedGameLog(game)))
  }

  def searchPlayer(name: String): Action[AnyContent] = Action {
    Ok(Json.toJson(db.searchPlayers(name)))
  }

  def getTeam(id: Int): Action[AnyContent] = Action {
    db.getTeamById(id).map { team =>
      Ok(Json.toJson(team))
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def searchTeam(query: String): Action[AnyContent] = Action {
    Ok(Json.toJson(db.searchTeams(query)))
  }

  def searchPark(query: String): Action[AnyContent] = Action {
    Ok(Json.toJson(db.searchParks(query)))
  }

  def playerBattingPlays(id: Int, milr: Option[Boolean]): Action[AnyContent] = Action {
    Ok(Json.toJson(db.getPlayerBattingPlays(id, milr)))
  }

  def playerPitchingPlays(id: Int, milr: Option[Boolean]): Action[AnyContent] = Action {
    Ok(Json.toJson(db.getPlayerPitchingPlays(id, milr)))
  }

  def playerFromSnowflake(snowflake: String): Action[AnyContent] = Action {
    db.getPlayerBySnowflake(snowflake).map { player =>
      Ok(Json.toJson(player))
    } getOrElse {
      NotFound("{\"error\": \"No player was found.\"}")
    }
  }

  def playersOnTeam(team: String): Action[AnyContent] = Action {
    db.getTeamByTag(team).map { team =>
      Ok(Json.toJson(db.getPlayersWithInfoOnTeam(team)))
    } getOrElse {
      Ok(Json.toJson(Seq[PlayerWithTypesAndInfo]()))
    }
  }

}
