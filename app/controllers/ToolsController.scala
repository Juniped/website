package controllers

import akka.util.ByteString
import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.http.HttpEntity
import play.api.libs.json.{Json, Writes}
import play.api.mvc._
import services.GlobalSettingsProvider

@Singleton
class ToolsController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def calculator: Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.tools.calc(db.getAllPlayersWithUsers, db.getAllBattingTypes, db.getAllPitchingTypes, db.getAllPitchingBonuses, db.getAllParksWithTeams)(req, user))
  }

  def calcData: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val allPlayers = Json.toJson(db.getAllPlayersWithUsers)(Writes.seq(PlayerWithUserIncludingTypeIdsWrite)).toString()
    val allBattingTypes = Json.toJson(db.getAllBattingTypes).toString()
    val allPitchingTypes = Json.toJson(db.getAllPitchingTypes).toString()
    val allPitchingBonuses = Json.toJson(db.getAllPitchingBonuses).toString()
    val allParks = Json.toJson(db.getAllParks).toString()
    val resp =
      s"""
         |window.fakebaseball = {};
         |window.fakebaseball.players = $allPlayers;
         |window.fakebaseball.battingTypes = $allBattingTypes;
         |window.fakebaseball.pitchingTypes = $allPitchingTypes;
         |window.fakebaseball.pitchingBonuses = $allPitchingBonuses;
         |window.fakebaseball.parks = $allParks;
       """.stripMargin
    Result(ResponseHeader(200), HttpEntity.Strict(ByteString(resp), Some("application/javascript")))
  }

}
