package services

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.DiscordTypes._
import model.{Game, PlayerWithUser}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class DiscordMessageService @Inject()(settingsProvider: GlobalSettingsProvider, config: Configuration, ws: WSClient) {

  private lazy val BOT_TOKEN = config.get[String]("discord.botToken")

  def sendABAlert(player: PlayerWithUser, game: Game): Unit = {
    val channel = settingsProvider.getSetting(settingsProvider.AB_PINGS_CHANNEL)
    channel.foreach { channel =>
      game.id36.foreach { id36 =>
        player.user.discord.foreach { snowflake =>
          Await.result(
            ws.url(s"https://discordapp.com/api/v6/channels/$channel/messages")
              .addHttpHeaders("Authorization" -> s"Bot $BOT_TOKEN")
              .post(Json.toJson(DiscordMessageRequest(content = Some(s"<@$snowflake>, you're up to bat!\n\nhttps://reddit.com/r/fakebaseball/comments/$id36/\n\nThis was sent from the website, so no baseball ping available (yet).")))),
            Duration(5, TimeUnit.SECONDS)
          )
        }
      }
    }
  }

}
